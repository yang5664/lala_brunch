<?php

namespace App\Admin\Controllers;

use App\Models\Notify;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class NotifyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '訊息推播管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Notify());

        $grid->column('id', __('Id'));
        $grid->column('shop_id', __('Shop id'));
        $grid->column('title', __('Title'));
        $grid->column('message', __('Message'));
        $grid->column('notify_at', __('Notify at'));
        $grid->column('status', __('Status'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Notify::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('shop_id', __('Shop id'));
        $show->field('title', __('Title'));
        $show->field('message', __('Message'));
        $show->field('notify_at', __('Notify at'));
        $show->field('status', __('Status'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Notify());

        $form->text('shop_id', __('Shop id'));
        $form->text('title', __('Title'));
        $form->text('message', __('Message'));
        $form->datetime('notify_at', __('Notify at'))->default(date('Y-m-d H:i:s'));
        $form->text('status', __('Status'));

        return $form;
    }
}
