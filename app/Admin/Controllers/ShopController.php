<?php

namespace App\Admin\Controllers;

use App\Models\Shop;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Admin\Controllers\Traits\Common;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Auth\Database\Administrator;

class ShopController extends AdminController
{
    use Common;

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '店家管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Shop());

        // $grid->column('id', __('Id'));
        // $grid->column('slug', __('Slug'));
        $grid->column('name', __('Name'));
        $grid->column('logo', __('Logo'))->display(function($val){
            if(empty($val)) {
                $val = asset("/images/empty-image.png");
            }
            return "<img src=\"/uploads/{$val}\" class=\"img-thumbnail\" style=\"width:50px;\" onerror=\" this.src='/images/empty-image.png' \">";
        });
        $grid->column('banner', __('Banner'))->display(function($val){
            if(empty($val)) {
                $val = asset("/images/empty-image.png");
            }
            return "<img src=\"/uploads/{$val}\" class=\"img-thumbnail\" style=\"width:100px;\" onerror=\" this.src='/images/empty-image.png' \">";
        });
        $grid->column('address', __('Address'))->editable();
        $grid->column('tel', __('Tel'))->editable();
        $grid->column('biz_time', __('Biz time'))->editable();
        $grid->column('latitude', __('Latitude'));
        $grid->column('longitude', __('Longitude'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Shop::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('slug', __('Slug'));
        $show->field('name', __('Name'));
        $show->field('logo', __('Logo'));
        $show->field('banner', __('Banner'));
        $show->field('address', __('Address'));
        $show->field('tel', __('Tel'));
        $show->field('biz_time', __('Biz time'));
        $show->field('latitude', __('Latitude'));
        $show->field('longitude', __('Longitude'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Shop::class, function (Form $form) {
            $form->tab('店家基本資料', function(Form $form){
                $form->text('slug', __('Slug'))->rules('required')->attribute('maxlength', 16);
                $form->text('name', __('Name'))->rules('required')->attribute('maxlength', 32);
                $form->image('logo', __('Logo'))->resize(120, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->uniqueName()->rules('required');
                $form->image('banner', __('Banner'))->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->uniqueName()->rules('required');
                $form->text('address', __('Address'))->rules('required')->attribute('maxlength', 128);
                $form->text('tel', __('Tel'))->rules('required')->attribute('maxlength', 32);
                $form->text('biz_time', __('Biz time'))->rules('required');
                $form->text('latitude', __('Latitude'))->help("留空將由系統根據地址轉換");
                $form->text('longitude', __('Longitude'));
            })->tab('管理人員', function($form){
                if(!$form->isEditing()) {
                    $form->text('username', "帳號")->rules('required|unique:shops,username,'.$form->model()->id)->attribute('maxlength', 16);
                } else {
                    $form->display('username', "帳號");
                }
                $form->password('password', "後台登入密碼")->rules('required');
            });
            //
            $form->saving(function (Form $form) {
                if (empty($form->latitude) && empty($form->longitude)) {
                    $geo = $this->GoogleAddrToGeo($form->address);
                    $form->latitude = $geo['lat'];
                    $form->longitude = $geo['long'];
                }
                if ($form->password && $form->model()->password != $form->password) {
                    $admin = Administrator::updateOrCreate([
                        'username' => $form->username
                    ],[
                        'name'      => $form->name,
                        'password'  => bcrypt($form->password)
                    ]);
                    $admin->roles()->sync([2]);
                }
            });
        });
    }
}
