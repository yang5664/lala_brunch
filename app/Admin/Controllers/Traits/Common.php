<?php

namespace App\Admin\Controllers\Traits;

use Illuminate\Http\File;

trait Common
{
    static $radio_option = ['1' => '是', '0'=> '否'];

    static $switch_open = [
        'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
        'off' => ['value' => 0, 'text' => '否', 'color' => 'warning'],
    ];

    static $switch_public = [
        'on'  => ['value' => 1, 'text' => '開啟', 'color' => 'primary'],
        'off' => ['value' => 0, 'text' => '關閉', 'color' => 'danger'],
    ];

    static $gender = ['f'=>'女', 'm'=>'男'];

    /**
     * save image
     */
    private function saveFile($file, $folder = "images", $fileName = null)
    {
        if ($file->isValid()) {
            if (empty($fileName)) {
                $fileName = time().'.'.$file->getClientOriginalExtension();
            }

            //
            $bad = array_merge(
                array_map('chr', range(0,31)),
                array("<", ">", ":", '"', "／","/", "\\", "|", "?", "*"));
            $fileName = str_replace($bad, "", $fileName);
            //
            if ('WIN' == substr(PHP_OS, 0, 3)) {
                $path = \Storage::disk('admin')->putFileAs($folder, new File($file), mb_convert_encoding($fileName, "BIG5"));
                // 連續檔案上傳, 來不及寫入
                sleep(1);
                return iconv("BIG5", "UTF-8", $path);
            } else {
                $path = \Storage::disk('admin')->putFileAs($folder, new File($file), $fileName);
                // 連續檔案上傳, 來不及寫入
                sleep(1);
                return $path;
            }
        }
    }

    /**
     * get file size
     */
    private function getFileSize($path) {
        return \Storage::disk('admin')->size($path);
    }

    /**
     * check form
     */
    private function checkForm() {
        // check if not fill form
        return isset(request()->all()['form']) ? true : false ;
        //
    }

    /**
     * 檔案大小
     */
    private function fileSize($file) {
        if(is_file($file) === false) {
            return '0 bytes';
        }
        $bytes = filesize($file);

        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     * 發送LINE Notify
     *
     * @return void
     */
    private function sendLineNotify($access_token, $message, $stickerId = 41, $stickerPackageId = 2) {

        if (is_array($message)) {
            $message = chr(13).chr(10) . implode(chr(13).chr(10), $message);
        }

        $apiUrl = "https://notify-api.line.me/api/notify";

        $params = [
            'message' => $message,
            'stickerPackageId' => $stickerPackageId,
            'stickerId' => $stickerId
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $access_token
        ]);
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        $output = curl_exec($ch);
        curl_close($ch);
    }

    /**
     * 地址轉經緯度
     *
     * @param [type] $addr
     * @return void
     */
    private function GoogleAddrToGeo($addr) {
        usleep(rand(1000, 3000)*1000);
        $USERAGENT="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36";
        $_addr = urlencode($addr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, $USERAGENT);
        curl_setopt($ch, CURLOPT_URL, "https://www.google.com/maps/place/$_addr");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        if (isset($_SERVER) && isset($_SERVER['HTTP_USER_AGENT'])) {
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }
        $output = curl_exec($ch);
        curl_close($ch);
        $regexStr = "/@\d*\.\d*,\d*\.\d*,/u";
        preg_match_all($regexStr, $output, $current_array);
        if (!empty($current_array[0])) {
            $strGeo = preg_replace("/@/i", "", end($current_array[0]));
            list($lat, $long) = explode(",", $strGeo);
            return compact('lat', 'long');
        } else {
            $regexStr = "/markers=\d*\.\d*%2C\d*\.\d*/u";
            preg_match_all($regexStr, $output, $current_array);
            if (!empty($current_array[0])) {
                $strGeo = preg_replace("/markers=/i", "", $current_array[0]);
                list($lat, $long) = explode("%2C", $strGeo[0]);
                return compact('lat', 'long');
            } else {
                // 還是找不到的話
                return [
                    'lat' => "-",
                    'long' => "-"
                ];
            }
        }

    }
    /**
     * 高德地圖
     *
     * @param [type] $addr
     * @return void
     */
    private function GaoDeAddrToGeo($addr) {
        usleep(rand(500, 1500)*1000);
        $USERAGENT="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36";
        $_addr = urlencode($addr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, $USERAGENT);
        curl_setopt($ch, CURLOPT_URL, "https://www.amap.com/service/poiInfo?query_type=TQUERY&pagesize=20&pagenum=1&qii=true&cluster_state=5&need_utd=true&utd_sceneid=1000&div=PC1000&addr_poi_merge=true&is_classify=true&zoom=14&city=710000&keywords=$_addr");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
    	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        $output = json_decode(curl_exec($ch), true);
        curl_close($ch);
        if (isset($output['data']) && isset($output['data']['poi_list'])) {
            $p = $output['data']['poi_list'][0];
            $lat = $p['latitude'];
            $long = $p['longitude'];
            return compact('lat', 'long');
        } else {
            return [
                'lat' => "-",
                'long' => "-"
            ];
        }
    }
}
