<?php

namespace App\Admin\Extensions\Form;

use Encore\Admin\Form\Field;

class CKEditor extends Field
{
    protected static $js = [
        '/vendor/libs/ckeditor/ckeditor.js',
        '/vendor/libs/ckeditor/adapters/jquery.js',
    ];

    protected $view = 'admin.extensions.ckeditor';

    public function render()
    {
        $prefix = config('lfm.url_prefix');
        $options = "{
            filebrowserImageBrowseUrl: '/{$prefix}?type=Images',
            filebrowserImageUploadUrl: '/{$prefix}/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/{$prefix}?type=Files',
            filebrowserUploadUrl: '/{$prefix}/upload?type=Files&_token='
          }";

        //$this->script = "CKEDITOR.replace('{$this->column}', $options);";
        $this->script = "$('textarea.{$this->getElementClassString()}').ckeditor($options);";
        return parent::render();
    }
}