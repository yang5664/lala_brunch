<?php

namespace App\Admin\Extensions\Form;

use Encore\Admin\Form\Field;
use Encore\Admin\Form\Field\PlainInput;

class InputMask extends Field
{
    use PlainInput;

    private $icon = 'fa-pencil-alt';

    protected $view = 'admin.extensions.input-mask';

    protected static $css = [
    ];

    protected static $js = [
        '/vendor/laravel-admin/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js',
    ];

    public function icon($icon) {
        $this->icon = $icon;
        return $this;
    }

    public function render()
    {
        $this->initPlainInput();

        $this->prepend("<i class='fa {$this->icon}'></i>")
            ->defaultAttribute('type', 'text')
            ->defaultAttribute('id', $this->id)
            ->defaultAttribute('name', $this->elementName ?: $this->formatName($this->column))
            ->defaultAttribute('value', old($this->column, $this->value()))
            ->defaultAttribute('class', 'inputmask form-control '.$this->getElementClassString())
            ->defaultAttribute('placeholder', $this->getPlaceholder());

        $this->addVariables([
            'prepend' => $this->prepend,
            'append'  => $this->append,
        ]);

        $this->script = <<<EOT

$(".inputmask").inputmask();

EOT;

        return parent::render();
    }
}