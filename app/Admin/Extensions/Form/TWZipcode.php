<?php

namespace App\Admin\Extensions\Form;

use Encore\Admin\Form\Field;

class TWZipcode extends Field
{
    protected $view = 'admin.extensions.twzipcode';

    protected static $css = [
    ];

    protected static $js = [
        '/vendor/libs/jquery-twzipcode/jquery.twzipcode.min.js',
    ];
    /**
     * Column name.
     *
     * @var string
     */
    protected $column = [];
    public function __construct($column, $arguments)
    {
        $this->column['zipcode'] = $column;
        $this->column['county'] = $arguments[0];
        $this->column['district'] = $arguments[1];

        $arguments = array_slice($arguments, 2);
        $this->label = $this->formatLabel($arguments);
        $this->id = $this->formatId($this->column);

    }
    
    public function prepare($value)
    {
        if ($value === '') {
            $value = null;
        }

        return $value;
    }

    public function render()
    {
        $name = $this->formatName($this->column);
        $zipcode = $this->column['zipcode'];
        $county = $this->column['county'];
        $district =  $this->column['district'];

        $this->script = <<<EOT

$('#twzipcode').twzipcode({
    'zipcodeName': '$zipcode',
    'countyName' : '$county',
    'districtName': '$district'
});

//
$("div[data-role='zipcode'] > input").attr('type', 'tel');

EOT;
        return parent::render();
    }
}