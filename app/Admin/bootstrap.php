<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */
use App\Admin\Extensions\Form\CKEditor;
use App\Admin\Extensions\Form\TWZipcode;
use App\Admin\Extensions\Form\Selectize;
use App\Admin\Extensions\Form\InputMask;
use App\Admin\Extensions\Form\LINENotifyBinder;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Admin;
// Encore\Admin\Form::forget(['map', 'editor']);

Form::extend('ckeditor', CKEditor::class);
Form::extend('zipcode', TWZipcode::class);
Form::extend('selectize', Selectize::class);
Form::extend('inputmask', InputMask::class);
Form::extend('linenotify', LINENotifyBinder::class);


Form::init(function (Form $form) {

    $form->disableEditingCheck();

    $form->disableCreatingCheck();

    $form->disableViewCheck();

    $form->tools(function (Form\Tools $tools) {
        $tools->disableDelete();
        $tools->disableView();
    });
});

Grid::init(function (Grid $grid) {

    // $grid->disableActions();

    // $grid->disablePagination();

    // $grid->disableCreateButton();

    $grid->disableFilter();

    $grid->disableRowSelector();

    $grid->disableColumnSelector();

    // $grid->disableTools();

    $grid->disableExport();

    $grid->actions(function (Grid\Displayers\Actions $actions) {
        $actions->disableView();
        // $actions->disableEdit();
        // $actions->disableDelete();
    });
});