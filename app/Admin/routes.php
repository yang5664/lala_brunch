<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');

    $router->resource('shops', ShopController::class);

    $router->resource('notifies', NotifyController::class);

    $router->resource('users', UserController::class);

    // LINE-Notify
    $router->get('supervisor/setting', 'SupervisorController@getSetting')->name('supervisor.setting');
    $router->put('supervisor/setting', 'SupervisorController@putSetting');
    $router->get('notify-cancel', 'SupervisorController@lineNotifyCancel')->name('admin-line-notify.cancel');
    $router->get('notify-callback', 'SupervisorController@lineNotifyCallback')->name('admin-line-notify.callback');
    $router->resource('supervisor', SupervisorController::class);
});
