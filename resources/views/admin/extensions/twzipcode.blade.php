<div class="{{$viewClass['form-group']}} {!! ($errors->has($errorKey['zipcode'].'zipcode') || $errors->has($errorKey['county'].'county') || $errors->has($errorKey['district'].'district')) ? 'has-error' : ''  !!}">

    <label for="{{$id['zipcode']}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}}">

        @include('admin::form.error')

        <div id="twzipcode">
            <div class="col-xs-4 col-md-4">
                <div class="form-group">
                    <div data-role="zipcode" data-style="form-control" data-value="{{ old($column['zipcode'], $value['zipcode']) }}"></div>
                </div>
            </div>
            <div class="col-xs-4 col-md-4">
                <div class="form-group">
                    <div data-role="county" data-style="form-control" data-value="{{ old($column['county'], $value['county']) }}"></div>
                </div>
            </div>
            <div class="col-xs-4 col-md-4">
                <div class="form-group">
                    <div data-role="district" data-style="form-control" data-value="{{ old($column['district'], $value['district']) }}"></div>
                </div>
            </div>
        </div>
    </div>
</div>